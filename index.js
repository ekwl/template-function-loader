const {getOptions} = require('loader-utils');
const validateOptions = require('schema-utils');
const schema = require('./schema.json');

module.exports = function(source) {
  const options = getOptions(this) || {};
  const callback = this.async();
  callback(null, eval(source)(options.data));
}